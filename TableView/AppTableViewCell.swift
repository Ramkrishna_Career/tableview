//
//  AppTableViewCell.swift
//  TableView
//
//  Created by SMIT on 12/18/16.
//  Copyright © 2016 Krishna. All rights reserved.
//

import UIKit
import SDWebImage
class AppTableViewCell: UITableViewCell
{
    @IBOutlet weak var appImageView: UIImageView!
    @IBOutlet weak var appNameLabel: UILabel!
    
   

    
    func updateUI(spices : SpecieServices) {
   
        appNameLabel.text = spices.name
        
        ImageLibraryManager.sharedInstance.downloadImage(imageURL: NSURL(string: spices.image!) as URL!, success: { (downloadedImage) in
                self.appImageView.image = downloadedImage
            }
            ,  failure: { (errorMessage) in
                self.appImageView.image = UIImage.init(named: "placeholder")
        })
        
        appImageView.sd_setImageWithPreviousCachedImage(with: (with: NSURL(string: spices.image!) as URL!), placeholderImage: UIImage.init(named: "placeholder"), options: .progressiveDownload, progress: nil, completed: nil)
        appNameLabel.layer.shadowColor = UIColor.black.cgColor
        appNameLabel.layer.shadowOffset = CGSize(width: 0, height: 0)
        appNameLabel.layer.shadowRadius = 6
        appNameLabel.layer.shadowOpacity = 1
    }
}
























