//
//  ImageLibrary.swift
//  TableView
//
//  Created by SMIT on 12/18/16.
//  Copyright © 2016 Krishna. All rights reserved.
//

import UIKit
import SDWebImage
class ImageLibraryManager {

    // Declare our 'sharedInstance' property
    static let sharedInstance = ImageLibraryManager()
    
    // Set an initializer -
    // it will only be called once
    init() {
        print("SomeManager initialized")
    }
  

    
    func downloadImage(imageURL: URL, success:@escaping (_ image : UIImage) -> Void, failure:@escaping (_ errorMessage: String) -> Void){
        
        let manager:SDWebImageManager = SDWebImageManager.shared()
        manager.downloadImage(with: imageURL,
                                          options: .progressiveDownload,
                                          progress: nil,
                                          completed: {(image, error, cached, finished, url) in
                                            if (error == nil && (image != nil) && finished) {
                                                // do something with image
                                                success(image!)
                                            }
                                        
        })

    }

}
