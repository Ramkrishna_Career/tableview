//
//  SpecieServices.swift
//  TableView
//
//  Created by SMIT on 12/18/16.
//  Copyright © 2016 Krishna. All rights reserved.
//

import Foundation

/* API Response to http http://swapi.co/api/species/3/ looks like:
{
  "name": "Wookiee",
  "classification": "mammal",
  "designation": "sentient",
  "average_height": "210",
  "skin_colors": "gray",
  "hair_colors": "black, brown",
  "eye_colors": "blue, green, yellow, brown, golden, red",
  "average_lifespan": "400",
  "homeworld": "http://swapi.co/api/planets/14/",
  "language": "Shyriiwook",
  "people": [
  "http://swapi.co/api/people/13/",
  "http://swapi.co/api/people/80/"
  ],
  "films": [
  "http://swapi.co/api/films/1/",
  "http://swapi.co/api/films/2/",
  "http://swapi.co/api/films/3/",
  "http://swapi.co/api/films/6/"
  ],
  "created": "2014-12-10T16:44:31.486000Z",
  "edited": "2015-01-30T21:23:03.074598Z",
  "url": "http://swapi.co/api/species/3/"
}
*/

import Foundation
import Alamofire

enum BackendError: Error {
  case urlError(reason: String)
  case objectSerialization(reason: String)
}

enum SpeciesFields: String {
  case Name = "name"
  case Classification = "classification"
  case AverageHeight = "average_height"
  case Image = "avatar"
}

class SpeciesWrapper {
  var species: [SpecieServices]?
  var count: Int?
  var next: String?
  var previous: String?
}

class SpecieServices {
  var idNumber: Int?
  var name: String?
  var classification: String?
  var averageHeight: Int?
 var image: String?
  
  required init(json: [String: Any]) {
    self.name = json[SpeciesFields.Name.rawValue] as? String
    self.classification = json[SpeciesFields.Classification.rawValue] as? String
    self.averageHeight = json[SpeciesFields.AverageHeight.rawValue] as? Int
    
    //Getting Random images from the function
    self.image = TIBApps.getRandomImage().imageName
    
    
    // TODO: more fields!
  }
  
  // MARK: Endpoints
  class func endpointForID(_ id: Int) -> String {
    return "https://swapi.co/api/species/\(id)"
  }
  class func endpointForSpecies() -> String {
    return "https://swapi.co/api/species/"
  }
  

  // GET / Read all species
  fileprivate class func getSpeciesAtPath(_ path: String, completionHandler: @escaping (Result<SpeciesWrapper>) -> Void) {
    // make sure it's HTTPS because sometimes the API gives us HTTP URLs
    guard var urlComponents = URLComponents(string: path) else {
      let error = BackendError.urlError(reason: "Tried to load an invalid URL")
      completionHandler(.failure(error))
      return
    }
    urlComponents.scheme = "https"
    guard let url = try? urlComponents.asURL() else {
      let error = BackendError.urlError(reason: "Tried to load an invalid URL")
      completionHandler(.failure(error))
      return
    }
    let _ = Alamofire.request(url)
      .responseJSON { response in
        if let error = response.result.error {
          completionHandler(.failure(error))
          return
        }
        let speciesWrapperResult = SpecieServices.speciesArrayFromResponse(response)
        completionHandler(speciesWrapperResult)
    }
  }
  
  class func getSpecies(_ completionHandler: @escaping (Result<SpeciesWrapper>) -> Void) {
    getSpeciesAtPath(SpecieServices.endpointForSpecies(), completionHandler: completionHandler)
  }
  
  class func getMoreSpecies(_ wrapper: SpeciesWrapper?, completionHandler: @escaping (Result<SpeciesWrapper>) -> Void) {
    guard let nextURL = wrapper?.next else {
      let error = BackendError.objectSerialization(reason: "Did not get wrapper for more species")
      completionHandler(.failure(error))
      return
    }
    getSpeciesAtPath(nextURL, completionHandler: completionHandler)
  }
  
  fileprivate class func speciesFromResponse(_ response: DataResponse<Any>) -> Result<SpecieServices> {
    guard response.result.error == nil else {
      // got an error in getting the data, need to handle it
      print(response.result.error!)
      return .failure(response.result.error!)
    }
    
    // make sure we got JSON and it's a dictionary
    guard let json = response.result.value as? [String: Any] else {
      print("didn't get species object as JSON from API")
      return .failure(BackendError.objectSerialization(reason:
        "Did not get JSON dictionary in response"))
    }
    
    let species = SpecieServices(json: json)
    return .success(species)
  }
  
  fileprivate class func speciesArrayFromResponse(_ response: DataResponse<Any>) -> Result<SpeciesWrapper> {
    guard response.result.error == nil else {
      // got an error in getting the data, need to handle it
      print(response.result.error!)
      return .failure(response.result.error!)
    }
    
    // make sure we got JSON and it's a dictionary
    guard let json = response.result.value as? [String: Any] else {
      print("didn't get species object as JSON from API")
      return .failure(BackendError.objectSerialization(reason:
        "Did not get JSON dictionary in response"))
    }
    
    let wrapper:SpeciesWrapper = SpeciesWrapper()
    wrapper.next = json["next"] as? String
    wrapper.previous = json["previous"] as? String
    wrapper.count = json["count"] as? Int
    
    
    
    
    var allSpecies: [SpecieServices] = []
    if let results = json["results"] as? [[String: Any]] {
      for jsonSpecies in results {
        let species = SpecieServices(json: jsonSpecies)
        allSpecies.append(species)
      }
    }
    wrapper.species = allSpecies
    return .success(wrapper)
  }
}




struct Images {
    
    var imageName: String
}

struct TIBApps {
    static func getRandomImage() -> Images {
        
    
        let image = [
            Images( imageName: "https://s24.postimg.org/a922bmf45/image.jpg"),
            Images( imageName: "https://s23.postimg.org/9zcmf38zv/image.jpg"),
            Images( imageName: "https://s27.postimg.org/7cwn8kor7/image.jpg"),
            Images( imageName: "https://s27.postimg.org/fehdp7qb7/image.jpg"),
            Images( imageName: "https://s24.postimg.org/910doao8l/image.jpg"),
            Images( imageName: "https://s27.postimg.org/j0o4acojn/image.jpg"),
            Images( imageName: "https://s23.postimg.org/6nay71ii3/image.jpg"),
            Images( imageName: "https://s27.postimg.org/713xcs8sz/image.jpg"),
            
            Images( imageName: "https://s27.postimg.org/8l9600zwj/image.jpg"),
            Images( imageName: "https://s27.postimg.org/er9ur9ug3/image.jpg"),
            
        ]
        
        
         let randomNum:UInt32 = arc4random_uniform(10) // range is 0 to 10
        
        let index:Int = Int(randomNum)
        
        return image[index]
    
  
    }

}




